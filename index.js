var express = require('express'); //Express Web Server
var bodyParser = require('body-parser'); //connects bodyParsing middleware
var formidable = require('formidable');
var path = require('path'); //used for file path
var fs = require('fs-extra'); //File System-needed for renaming file etc

var app = express();
app.use(express.static(path.join(__dirname, 'uploads')));
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

/* ========================================================== 
 bodyParser() required to allow Express to see the uploaded files
============================================================ */

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.post('/upload', function(req, res, next) {
  var form = new formidable.IncomingForm();
  //Formidable uploads to operating systems tmp dir by default
  form.uploadDir = './uploads'; //set upload directory
  form.keepExtensions = true; //keep file extension

  form.parse(req, function(err, fields, files) {
    console.log('fields:', fields);
    //TESTING
    console.log('file size: ' + JSON.stringify(files.fileUploaded.size));
    console.log('file path: ' + JSON.stringify(files.fileUploaded.path));
    console.log('file name: ' + JSON.stringify(fields.filename));
    console.log('file type: ' + JSON.stringify(files.fileUploaded.type));
    console.log(
      'LastModifiedDate: ' + JSON.stringify(files.fileUploaded.lastModifiedDate)
    );

    //Formidable changes the name of the uploaded file
    //Rename the file to its original name
    fs.rename(files.fileUploaded.path, './uploads/' + fields.filename, function(
      err
    ) {
      if (err) throw err;
      console.log('renamed complete');
    });
    res.sendFile(__dirname + '/index.html');
  });
});

var server = app.listen(3000, function() {
  console.log('Listening on port %d', server.address().port);
});
